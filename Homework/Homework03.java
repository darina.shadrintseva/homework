package Homework;

import java.util.Scanner;

/* ## Робота з рядками

## Завдання

Написати консольну програму "планувальник завдань на тиждень".

#### Технічні вимоги:
- Створити двовимірний масив рядків розмірністю 7х2
  `String[][] scedule = new String[7][2]`
- Заповніть матрицю значеннями день тижня: головне завдання на цей день:

  `scedule[0][0] = "Sunday";`
  `scedule[0][1] = "do home work";`
  `scedule[1][0] = "Monday";`
  `scedule[1][1] = "go to courses; watch a film";`

- Використовуючи цикл та оператор switch, запитайте у користувача день тижня в консолі, залежно від введення:
   1) програма: `Please, input the day of the week: `
   користувач вводить коректний день тижня (f.e. `Monday`)
   програма виводить на екран `Your tasks for Monday: go to courses; watch a film.`;
   програма йде на наступну ітерацію;
   2) програма: `Please, input the day of the week: `
   користувач вводить некоректний день тижня (f.e. `%$=+!11=4`)
   програма виводить на екран `Sorry, I don't understand you, please try again.`; програма йде на наступну ітерацію до
   успішного введення;
   3) програма: `Please, input the day of the week: `
   користувач виводить команду виходу `exit`
   програма виходить із циклу і коректно завершує роботу.
- Завдання повинно бути виконане за допомогою масивів (**НЕ** використовуйте інтерфейси `List`, `Set`, `Map`).

  Зверніть увагу: програма повинна приймати команди як у нижньому регістрі (`monday`) так і у верхньому (`MONDAY`) і
  врахуйте, що користувач міг випадково після дня тижня ввести пробіл.
 */
public class Homework03 {
    public static void main(String[] args) {

            Scanner scanner = new Scanner(System.in);

            String[][] scedule = new String[7][2];
            scedule[0][0] = "Sunday";
            scedule[0][1] = "do home work";
            scedule[1][0] = "Monday";
            scedule[1][1] = "go to courses; watch a film";

            while (true) {
                System.out.println("Please, input the day of the week");
                String day = scanner.nextLine();
                String exit = "Exit";
                boolean ignoreCase = true;

                if (day.regionMatches(ignoreCase, 0, scedule[0][0], 0, 6)){
                    System.out.println("Your tasks for " + scedule[0][0] + " : " + scedule[0][1]);
                } else if (day.regionMatches(ignoreCase, 0, scedule[1][0], 0, 6)) {
                    System.out.println("Your tasks for " + scedule[1][0] + " : " + scedule[1][1]);
                } else if (day.regionMatches(ignoreCase, 0, exit, 0, 4)) {
                    break;
                } else {
                    System.out.println("Sorry, I don't understand you, please try again.");
                }

            }

        }
    }