package Homework;

import java.util.Random;
import java.util.Scanner;

/* ## Масиви

## Завдання

Написати програму "стрільба по цілі".

#### Технічні вимоги:

- Даний квадрат 5х5, де програма випадковим чином ставить ціль.
- Перед початком гри на екран виводиться текст: `All Set. Get ready to rumble!`.
- Сам процес гри обробляється у нескінченному циклі.
- гравцеві пропонується ввести лінію для стрільби; програма перевіряє, щоб було введено число, і введена лінія
  знаходиться в межах ігрового поля (1-5). У випадку, якщо гравець помилився, пропонує ввести число ще раз.
- Гравцю пропонується ввести стовпчик для стрільби (має проходити аналогічну перевірку).
- Після кожного пострілу необхідно показувати оновлене ігрове поле у консолі. Клітинки, куди гравець вже стріляв, слід
  зазначити як `*`.
- Гра закінчується при попаданні в ціль. Наприкінці гри вивести в консоль фразу `You have won!`, а також ігрове поле.
  Уражену ціль відзначити як `x`.
- Завдання повинно бути виконане за допомогою масивів (**НЕ** використовуйте інтерфейси `List`, `Set`, `Map`).
Приклад виведення в консоль:

` 0 | 1 | 2 | 3 | 4 | 5 |`
` 1 | - | - | - | - | - |`
` 2 | - | * | * | - | - |`
` 3 | * | - | - | * | - |`
` 4 | - | - | - | - | * |`
` 5 | * | - | * | - | - |`
*/

/*        Питання:
        1. Чи можна якимось чином записати в один рядок
           board[1][1] - board[5][5] = '-' (включно) ?
        2. При вводі другого числа від користувача в разі помилки перекидає знову до вводу першого числа так як один
           цикл, але якщо закрити цикл з першим числом, то перемінна з нього не доступна в наступних циклах. Як вийти
           з цієї ситуації?

        */

public class Homework02 {
    public static void drawBoard(char[][] board) {
        for (int y = 0; y < board.length; y++) {
            for (int x = 0; x < board.length; x++) {
                System.out.printf("%c | ", board[y][x]);
            }
            System.out.println();
        }
    }
    public static void main(String[] args) {

        Random random = new Random();
        Scanner scanner = new Scanner(System.in);

        final int SIZE = 6;
        char[][] board = new char[SIZE][SIZE];
        board[0][0] = '0';
        board[1][0] = board[0][1] = '1';
        board[2][0] = board[0][2] = '2';
        board[3][0] = board[0][3] = '3';
        board[4][0] = board[0][4] = '4';
        board[5][0] = board[0][5] = '5';
        board[1][1] = board[1][2] = board[1][3] = board[1][4] = board[1][5] = '-';
        board[2][1] = board[2][2] = board[2][3] = board[2][4] = board[2][5] = '-';
        board[3][1] = board[3][2] = board[3][3] = board[3][4] = board[3][5] = '-';
        board[4][1] = board[4][2] = board[4][3] = board[4][4] = board[4][5] = '-';
        board[5][1] = board[5][2] = board[5][3] = board[5][4] = board[5][5] = '-';

        int random_num_y = random.nextInt(5) + 1;
        int random_num_x = random.nextInt(5) + 1;
//        board[random_num_y][random_num_x] = 'X';

        System.out.print("All Set. Get ready to rumble!");
        String go = scanner.nextLine();

        while (true) {
            System.out.print("Enter a row number from 1 to 5: ");
            int player_num_y = scanner.nextInt();
            if (player_num_y < 1 || player_num_y > 5) {
                System.out.println("Error!");
            } else {
                System.out.print("Enter a column number from 1 to 5: ");
                int player_num_x = scanner.nextInt();
                if (player_num_x < 1 || player_num_x > 5) {
                    System.out.println("Error!");
                } else {
                    if (player_num_y != random_num_y && player_num_x != random_num_x) {
                        System.out.println("Try again!");
                        board[player_num_y][player_num_x] = '*';
                        drawBoard(board);

                    } else if (player_num_y != random_num_y || player_num_x != random_num_x) {
                        System.out.println("Try again!");
                        board[player_num_y][player_num_x] = '*';
                        drawBoard(board);

                    } else {
                        System.out.println("You have won!");
                        board[player_num_y][player_num_x] = 'X';
                        drawBoard(board);

                        break;
                    }
                }
            }
        }
    }
}