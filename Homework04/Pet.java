package Homework04;

import java.util.Arrays;
import java.util.Objects;

public class Pet {

    //  Поля

    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    //  Методи

    public void eat() {
        System.out.println("Я їм!");
    }
    public void respond(String nickname) {
        System.out.println("Привіт, хазяїн. Я - " + nickname + ". Я скучив!");
    }
    public void foul() {
        System.out.println("Потрібно добре замести сліди...");
    }

    //    Сетери/Гетери

    public String getSpecies() {
        return species;
    }
    public void setSpecies(String species) {
        this.species = species;
    }
    public String getNickname() {
        return nickname;
    }
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public int getTrickLevel() {
        return trickLevel;
    }
    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }
    public String[] getHabits() {
        return habits;
    }
    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    //  Перевизначення методу toString => dog{nickname='Rock', age=5, trickLevel=75, habits=[eat, drink, sleep]}

    @Override
    public String toString() {
        return new StringBuilder()
                .append(species)
                .append("{nickname='").append(nickname)
                .append("', age=").append(age)
                .append(", trickLevel=").append(trickLevel)
                .append(", habits=").append(Arrays.toString(habits))
                .append('}')
                .toString();
    }

    //  Перевизначeння методів equals() та hashCode()

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Pet pet = (Pet) obj;
        return age == pet.age && Objects.equals(nickname, pet.nickname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nickname, age);
    }

    //  Конструктори

    private String[] combineHabits(String habit, String[] habits) {
        if (habits.length == 0) return new String[] { habit };
        String[] allHabits = new String[1 + habits.length];
        allHabits[0] = habit;
        System.arraycopy(habits, 0, allHabits, 1, habits.length);
        return allHabits;
    }
    public Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }
    public Pet(String species, String nickname, int age, int trickLevel, String habit, String... habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.combineHabits(habit, habits);
    }
        public Pet(){
    }

    //  Вивід

        public static void main(String[] args) {
        Pet p1 = new Pet();
        System.out.println(p1);
    }
}