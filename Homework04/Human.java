package Homework04;

import java.util.Arrays;
import java.util.Objects;

public class Human {

    //  Поля

    private String name;
    private String surname;
    private int year;
    private int iq;
    private Family family;
    private String[][] schedule;

    //  Методи

    public void greetPet() {
        String petNickname = family.getPet().getNickname();

        System.out.println("Привіт, " + petNickname + "!");
    }
    public void describePet() {
        int petTrickLevel = family.getPet().getTrickLevel();
        String petSpecies = family.getPet().getSpecies();
        int petAge = family.getPet().getAge();

        if (petTrickLevel <= 50) {
            System.out.println("У мене є " + petSpecies + " , їй " + petAge + " років, він майже не хитрий.");
        } else {
            System.out.println("У мене є " + petSpecies + " , їй " + petAge + " років, він дуже хитрий.");
        }
    }

    //    Сетери/Гетери

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public int getYear() {
        return year;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public int getIq() {
        return iq;
    }
    public void setIq(int iq) {
        this.iq = iq;
    }
    public Family getFamily() {
        return family;
    }
    public void setFamily(Family family) {
        this.family = family;
    }
    public String[][] getSchedule() {
        return schedule;
    }
    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    /*  Перевизначення методу toString 1 => Human{name='Michael', surname='Karleone', year=1977, iq=90, mother=Jane Karleone,
            father=Vito Karleone, pet=dog{nickname=' Rock', age=5, trickLevel=75, habits=[eat, drink, sleep]}}
        Перевизначення методу toString 2 => Human{name='Name', surname='Surname', year=1, iq=1, schedule=[[day, task], [day_2, task_2]]}
         */

    @Override
    public String toString() {
        return new StringBuilder()
                .append("Human{name='").append(name)
                .append("', surname='").append(surname)
                .append("', year=").append(year)
                .append(", iq=").append(iq)
                .append(", schedule=").append(Arrays.toString(schedule))
                .append('}')
                .toString();
    }

    //  Перевизначeння методів equals() та hashCode()

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Human human = (Human) object;
        return year == human.year && Objects.equals(name, human.name) && Objects.equals(surname, human.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year);
    }

    // Конструктори

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }
    public Human(String name, String surname, int year, Family family) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.family = family;
    }
    public Human(String name, String surname, int year, int iq, Family family, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.family = family;
        this.schedule = schedule;
    }
    public Human() {
    }

    //  Вивід

    public static void main(String[] args) {
        Human h1 = new Human();
        System.out.println(h1);
    }
}