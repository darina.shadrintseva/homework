package Homework04;

import java.util.Arrays;
import java.util.Objects;

public class Family {

    // Поля

    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;

    //  Методи

    public Human[] add(Human arr[], Human x) {
        int i;
        int n = arr.length;
        Human newArr[] = new Human[n + 1];

        for (i = 0; i < arr.length; i++) {
            newArr[i] = arr[i];
            newArr[n] = x;
        }
        return newArr;
    }
    public void addChild (Human child) {
        children = add(children, child);
        setChildren(children);
    }

    //    Сетери/Гетери

    public Human getMother() {
        return mother;
    }
    public void setMother(Human mother) {
        this.mother = mother;
    }
    public Human getFather() {
        return father;
    }
    public void setFather(Human father) {
        this.father = father;
    }
    public Human[] getChildren() {
        return children;
    }
    public void setChildren(Human[] children) {
        this.children = children;
    }
    public Pet getPet() {
        return pet;
    }
    public void setPet(Pet pet) {
        this.pet = pet;
    }

    //  Перевизначення методу toString

    @Override
    public String toString() {
        return new StringBuilder()
                .append("Family{")
                .append(getPet().toString())
                .append(getMother().toString())
                .append(getFather().toString())
                .append(Arrays.toString(children))
                .append('}')
                .toString();
    }

    //  Перевизначeння методів equals() та hashCode()

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Family family = (Family) object;
        return Objects.equals(mother, family.mother) && Objects.equals(father, family.father);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father);
    }

    //  Конструктор

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        pet = new Pet();
        children = new Human[0];
    }
    public Family() {
    }

    //  Вивід

    public static void main(String[] args) {
        Family f1 = new Family();
        System.out.println(f1);
    }
}
